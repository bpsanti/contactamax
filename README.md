# contactamax

Informações Pessoais:
Bruno Santi
bpsanti@outlook.com

Prova ContactaMax:

Versão PHP: 7.2.8;
Versão MYSQL: 14.14;

Informações básicas:

Utilizei o sistema de autenticação padrão do Laravel. Não criei uma seed para um usuário padrão porque é possivel
a realização de um registro por ali mesmo.

Sobre os requisitos:
1. CRUD para os produtos - OK
2. Tela para adicionar produtos ao estoque - OK
3. Tela para dar baixa aos produtos - OK
4. O sistema deve possuir uma api - OK, porém como tive poucas experiências com API, não consegui estudar a melhor forma de realizar a autenticação.
    4.1. Baixar Produtos - Exemplo (O SKU do produto deve estar cadastrado no sistema):    
        curl -X POST http://localhost:90/api/baixar-produtos \
          -H "Accept: application/json" \
          -H "Content-Type: application/json" \
          -d '{"product_sku": "CC-TEST-1", "quantity": "120"}'

    4.2. Adicionar Produtos - Exemplo (O SKU do produto deve estar cadastrado no sistema):    
        curl -X POST http://localhost:90/api/adicionar-produtos \
         -H "Accept: application/json" \
         -H "Content-Type: application/json" \
         -d '{"product_sku": "CC-TEST-1", "quantity": "20"}'

5. Relatorio dos produtos movimentados por dia:
    5.1. Quantos e quais produtos foram adicionados ao estoque - OK;
    5.2. Quantos e quais produtos foram baixados do estoque - OK;
    5.3. Adição/Remoção por API - OK;
    5.4. Aviso de estoque baixo do produto - OK;

6. Sistema protegido por login - OK

7. Validação de quantidade de produtos - OK
8. Não poderão ser cadastrados produtos com SKU duplciado - OK
