<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMovementsTable extends Migration
{
    /**
     * Migracao para a tabela de movimentação de produtos
     * o objetivo é guardar cada entrada/saida.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('movements', function (Blueprint $table) {
            $table->increments('id');
            $table->string('product_sku');
            $table->unsignedInteger('quantity');
            $table->string('type'); // IN E OUT
            $table->unsignedInteger('user_id');
            $table->string('origin');
            $table->timestamps();

            $table->foreign('product_sku')->references('sku')->on('products');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('movements');
        Schema::dropIfExists('products_movements');
    }
}
