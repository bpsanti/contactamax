<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $dayMovements = App\Movement::select('product_sku', \DB::raw('SUM(quantity) as quantity'), 'type', 'origin', 'movements.created_at')
                                        ->where('movements.created_at', '>=', \DB::raw('current_date()'))
                                        ->where('movements.created_at', '<', \DB::raw('current_date() + 1'))
                                        ->groupby('product_sku', 'type', 'origin', 'movements.created_at')
                                        ->orderby('type', 'product_sku')
                                        ->get();

        $products = App\Product::where('stock', '<', 100)
                                ->get();

        $day = date('d/m/Y');

        return view('dashboard', compact('dayMovements', 'products', 'day'));
    }

    public function get(Request $data) {
        $dayMovements = App\Movement::select('product_sku', \DB::raw('SUM(quantity) as quantity'), 'type', 'origin', 'movements.created_at')
                                        ->where('movements.created_at', '>=', $data['movement-date'])
                                        ->where('movements.created_at', '<', date('Y-m-d', strtotime($data['movement-date']. '+1 days')))
                                        ->groupby('product_sku', 'type', 'origin', 'movements.created_at')
                                        ->orderby('type', 'product_sku')
                                        ->get();

        $products = App\Product::where('stock', '<', 100)
                                ->get();

        $day = date('d/m/Y', strtotime($data['movement-date']));

        return view('dashboard', compact('dayMovements', 'products', 'day'));
    }
}
