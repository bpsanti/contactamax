<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App;

class ProductController extends Controller {
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Retorna a pagina de visualizacao dos produtos
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $products = App\Product::get();

        return view('product', compact('products'));
    }

    /**
     * Retorna a pagina com o formulario para adicao dos produtos
     *
     * @return \Illuminate\Http\Response
     */
    public function get(String $product_sku) {
        $product = App\Product::where('sku', $product_sku)->get();

        return view('product.get_product', compact('product'));
    }

    /**
     * Retorna a pagina com o formulario para adicao dos produtos
     *
     * @return \Illuminate\Http\Response
     */
    public function showProductAddForm() {
        return view('product.add_product');
    }

    /**
     * Valida e se valido, então cria um novo produto
     *
     * @param  Request $data
     * @return \App\Product
     */
    protected function create(Request $data) {
        $errors = $data->validate([
            'product_sku'   => 'required|string|max:16|unique:products,sku',
            'product_name'  => 'required|string|max:255',
        ]);

        App\Product::create([
            'sku'       => $data['product_sku'],
            'name'      => $data['product_name'],
            'stock'     => 0,
            'user_id'   => $data['user_id'],
        ]);

        return redirect('/products');
    }


    /**
     * Valida e se valido, faz o update do produto
     *
     * @param  Request $data
     * @return \App\Product
     */
    protected function update(Request $data) {
        $errors = $data->validate([
            'product_name'  => 'required|string|max:255',
            'product_sku'   => 'required|string|max:16|unique:products,sku,'.$data->product_id,
        ]);

        $product = App\Product::find($data->product_id);

        $product->sku   = $data->product_sku;
        $product->name  = $data->product_name;

        $product->save();

        return redirect('/products');
    }

    protected function delete(Request $data) {
        $product = App\Product::find($data->product_id);

        $t = App\Movement::where('product_sku', '=', $data['product_sku'])->get();

        if (sizeof($t) === 0)
            $product->delete();

        return redirect('/products');
    }

}
