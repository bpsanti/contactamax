<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;

class MovementController extends Controller {
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Retorna a pagina de visualizacao dos produtos
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $movements = App\Movement::select('*', 'movements.created_at as date')
                                    ->join('products', 'movements.product_sku', 'products.sku')
                                    ->orderby('date')
                                    ->get();

        return view('movement', compact('movements'));
    }


    /**
     * Retorna a pagina com o formulario para adicao dos produtos
     *
     * @return \Illuminate\Http\Response
     */
    public function showMovementAddInForm() {
        $type = 'in';

        return view('movement.add_movement', compact('type'));
    }


    /**
     * Retorna a pagina com o formulario para adicao dos produtos
     *
     * @return \Illuminate\Http\Response
     */
    public function showMovementAddOutForm() {
        $type = 'out';

        return view('movement.add_movement', compact('type'));
    }

    /**
     * Valida e se valido, então cria uma nova movimentacao
     *
     * @param  Request $data
     * @return \App\Movement
     */
    protected function create(Request $data) {
        // Validacao de campos
        $errors = \Validator::make($data->all(), [
            'movement_sku'  => 'required|string|max:16|exists:products,sku',
            'movement_qty'  => 'required|numeric|min:1',
        ]);

        // Validacao de estoque
        $errors->after(function ($errors) use ($data) {
            if ($data['movement_type'] === 'out') {
                $product = App\Product::where('sku', $data['movement_sku'])->get();

                if ($product[0]->stock < $data['movement_qty']) {
                    $errors->errors()->add('movement_qty', 'O produto possui apenas ' .$product[0]->stock. ' unidades.');
                }
            }
        });

        $errors->validate();

        // Atualiza o estoque do produto dependendo da operação
        if ($data['movement_type'] == "in") {
            $product = App\Product::where('sku', $data->movement_sku)->get();

            $product[0]->stock += $data['movement_qty'];

            $product[0]->update();
        } else {
            $product = App\Product::where('sku', $data->movement_sku)->get();

            $product[0]->stock -= $data['movement_qty'];

            $product[0]->update();
        }

        // Cria o produto
        App\Movement::create([
            'product_sku'   => $data['movement_sku'],
            'quantity'      => $data['movement_qty'],
            'type'          => $data['movement_type'],
            'user_id'       => $data['user_id'],
            'origin'        => 'system',
        ]);

        return redirect('/movements');
    }


}
