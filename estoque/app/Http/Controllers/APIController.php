<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\Movement as MovementResource;
use App;

class APIController extends Controller {
    public function show($id) {
        return new MovementResource(App\Movement::find($id));
    }

    public function addInAPI(Request $request) {
        $product = App\Product::where('sku', $request['product_sku'])->get();
        $product[0]->stock += $request['quantity'];
        $product[0]->update();

        $movement = App\Movement::create([
                'product_sku'   => $request['product_sku'],
                'quantity'      => $request['quantity'],
                'type'          => 'in',
                'user_id'       => '1',
                'origin'        => 'API',
            ]);


        return response()->json($movement, 201);
    }

    public function addOutAPI(Request $request) {
        $product = App\Product::where('sku', $request['product_sku'])->get();

        if ($product[0]->stock < $request['quantity']) {

            return response()->json('O produto possui apenas ' .$product[0]->stock. ' unidades.', 201);
        } else {
            $product[0]->stock -= $request['quantity'];
            $product[0]->update();

            $movement = App\Movement::create([
                    'product_sku'   => $request['product_sku'],
                    'quantity'      => $request['quantity'],
                    'type'          => 'out',
                    'user_id'       => '1',
                    'origin'        => 'API',
                ]);


            return response()->json($movement, 201);
        }
    }
}
