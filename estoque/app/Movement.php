<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Movement extends Model {
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'product_sku', 'quantity', 'type', 'user_id', 'origin'
    ];
}
