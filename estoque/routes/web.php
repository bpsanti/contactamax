<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();


Route::get  ('/',                   'DashboardController@index');
Route::get  ('/dashboard',          'DashboardController@index');
Route::get  ('/dashboard/{day}','DashboardController@get');

// rotas relacionadas ao produto
Route::get  ('/products/',          'ProductController@index');
Route::post ('/products/put',       'ProductController@update');
Route::post ('/products/post',      'ProductController@create');
Route::get  ('/products/form',      'ProductController@showProductAddForm');
Route::post ('/products/delete',    'ProductController@delete');
Route::get  ('/products/get/{sku}', 'ProductController@get');

// rotas relacionas aos movimentos
Route::get  ('/movements/',         'MovementController@index');
Route::post ('/movements/post',     'MovementController@create');
Route::get  ('/movements/form/in',  'MovementController@showMovementAddInForm');
Route::get  ('/movements/form/out', 'MovementController@showMovementAddOutForm');
