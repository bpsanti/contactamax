@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Adicionar Produto</div>

                <div class="card-body">
                    <form method="POST" action="{{ url('products/post') }}" aria-label="{{ __('Register') }}">
                        @csrf
                        <input name="user_id" type="hidden" value="{{Auth::user()->id}}"></input>

                        <div class="form-group row">
                            <label for="product_sku" class="col-md-4 col-form-label text-md-right">SKU</label>

                            <div class="col-md-6">
                                <input id="product_sku" type="text" class="form-control{{ $errors->has('product_sku') ? ' is-invalid' : '' }}"
                                    name="product_sku" value="{{ old('product_sku') }}" maxlength="16" required autofocus>

                                @if ($errors->has('product_sku'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('product_sku') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="product_name" class="col-md-4 col-form-label text-md-right">Nome do produto</label>

                            <div class="col-md-6">
                                <input id="product_name" type="text" class="form-control{{ $errors->has('product_name') ? ' is-invalid' : '' }}"
                                    name="product_name" value="{{ old('product_name') }}" required autofocus>

                                @if ($errors->has('product_name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('product_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Cadastrar
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
