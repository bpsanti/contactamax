@extends('layouts.app')

@section('content')

<link href="{{ asset('/css/product.css') }}" type="text/css" rel="stylesheet">

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    <div class="header">
                        <div class='product-new'>
                            Resumo das movimentações do dia {{ $day }}:
                        </div>

                        <form action={{url('/dashboard/get')}} class="product-search">
                            <button type="submit" class="btn btn-primary">
                                Buscar
                            </button>

                            <input type="date" id="movement-date" name="movement-date" class="form-control" required>
                        </form>

                    </div>

                    <br><br>

                    <table  class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>SKU</th>
                                <th>Qtd.</th>
                                <th>Tipo</th>
                                <th>Origem</th>
                                <th>Data</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php ($cont = 1)
                            @foreach ($dayMovements as $movement)
                                <tr>
                                    <td> {{ $cont++ }} </td>
                                    <td> {{ $movement->product_sku }} </td>
                                    <td> {{ $movement->quantity }} </td>
                                    <td> {{ $movement->type }} </td>
                                    <td> {{ $movement->origin }} </td>
                                    <td> {{ date('d/m/Y', strtotime($movement->created_at)) }} </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>

                    <br>

                    <strong> Atenção: </strong> Produtos abaixo de 100 unidades no estoque!

                    <br><br>

                    <table  class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>SKU</th>
                                <th>Produto</th>
                                <th>Quantidade</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php ($cont = 1)
                            @foreach ($products as $product)
                                <tr>
                                    <td>{{ $cont++ }}</td>
                                    <td>{{ $product->sku }}</td>
                                    <td>{{ $product->name }}</td>
                                    <td>{{ $product->stock }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
