@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                @if($type == 'in')
                    <div class="card-header">Entrada de Mercadorias</div>
                @else
                    <div class="card-header">Saída de Mercadorias</div>
                @endif

                <div class="card-body">
                    <form method="POST" action="{{ url('movements/post') }}" aria-label="{{ __('Register') }}">
                        @csrf
                        <input name="user_id" type="hidden" value="{{Auth::user()->id}}"></input>
                        <input name="movement_type" type="hidden" value="{{ $type }}"></input>

                        <div class="form-group row">
                            <label for="movement_sku" class="col-md-4 col-form-label text-md-right">SKU do produto</label>

                            <div class="col-md-6">
                                <input id="movement_sku" type="text" class="form-control{{ $errors->has('movement_sku') ? ' is-invalid' : '' }}"
                                    name="movement_sku" value="{{ old('movement_sku') }}" maxlength="16" required autofocus>

                                @if ($errors->has('movement_sku'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('movement_sku') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="movement_qty" class="col-md-4 col-form-label text-md-right">Quantidade</label>

                            <div class="col-md-6">
                                <input id="movement_qty" type="number" class="form-control{{ $errors->has('movement_qty') ? ' is-invalid' : '' }}"
                                    name="movement_qty" value="{{ old('movement_qty') }}" required autofocus>

                                @if ($errors->has('movement_qty'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('movement_qty') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                @if($type == 'in')
                                    <button type="submit" class="btn btn-primary">
                                        Cadastrar Entrada
                                    </button>
                                @else
                                    <button forma type="submit" class="btn btn-primary">
                                        Cadastrar Saída
                                    </button>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
