@extends('layouts.app')

@section('content')

<link href="{{ asset('/css/product.css') }}" type="text/css" rel="stylesheet">

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Produtos</div>

                <div class="card-body">
                    <div class="header">
                        <form class="product-new" action="{{ url('products/form') }}">
                            <button type="submit" class="btn btn-primary">
                                Novo
                            </button>
                        </form>

                    </div>

                    <br><br><br>

                    <table  class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>SKU</th>
                                <th>Produto</th>
                                <th>Quantidade</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php ($cont = 1)
                            @foreach ($products as $product)
                                <tr>
                                    <td><a href="{{ url('products/get/'.$product->sku) }}"> {{ $cont++ }} </a></td>
                                    <td><a href="{{ url('products/get/'.$product->sku) }}"> {{ $product->sku }} </a></td>
                                    <td><a href="{{ url('products/get/'.$product->sku) }}"> {{ $product->name }} </a></td>
                                    <td><a href="{{ url('products/get/'.$product->sku) }}"> {{ $product->stock }} </a></td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
