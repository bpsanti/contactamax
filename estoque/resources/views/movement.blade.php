@extends('layouts.app')

@section('content')

<link href="{{ asset('/css/product.css') }}" type="text/css" rel="stylesheet">

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Últimas Movimentações</div>

                <div class="card-body">
                    <div class="header">
                        <form class="product-new">
                            <button formaction="{{ url('movements/form/in') }}" type="submit" class="btn btn-primary">
                                Entrada
                            </button>

                            <button formaction="{{ url('movements/form/out') }}" type="submit" class="btn btn-primary">
                                Saída
                            </button>
                        </form>
                    </div>

                    <br><br><br>

                    <table  class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>SKU</th>
                                <th>Produto</th>
                                <th>Tipo</th>
                                <th>Origem</th>
                                <th>Qtd.</th>
                                <th style="float: right;">Data</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php ($cont = 1)
                            @foreach ($movements as $movement)
                                <tr>
                                    <td> {{ $cont++ }} </td>
                                    <td> {{ $movement->product_sku }} </td>
                                    <td> {{ $movement->name }} </td>
                                    <td> {{ $movement->type }} </td>
                                    <td> {{ $movement->origin }} </td>
                                    <td> {{ $movement->quantity }} </td>
                                    <td style="float: right;"> {{ date("d/m/Y", strtotime($movement->date)) }} </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
